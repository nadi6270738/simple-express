// Third-party modules
const express = require('express')
const bodyParser = require('body-parser')
const mongoose = require('mongoose')
const Task = require('./models/taskModel')

const app = express()
const port = 3000

app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

const dbUrl = 'mongodb://localhost:27017/todo'
mongoose.connect(dbUrl, {
    useNewUrlParser: true, useUnifiedTopology: true
})

app.get('/', (req, res) => {
    Task.find().then(tasks => {
        res.status(200).json({
            status: true,
            message: 'Task berhasil ditampilkan',
            data: tasks,
        })
    })
})

app.post('/', (req, res) => {
    Task.create({
        task: req.body.task,
    }).then(task => {
        res.status(201).json({
            status: true,
            message: 'Task berhasil ditambahkan',
            data: task,
        })
    })
})

app.get('/:id', (req, res) => {
    const id = req.params.id
    Task.findById(id).then(task => {
        res.status(200).json({
            status: true,
            message: 'Task berhasil ditampilkan',
            data: task,
        })
    })
})

app.put('/:id', (req, res) => {
    Task.findByIdAndUpdate(req.params.id, {
        task: req.body.task,
        status: req.body.status,
    }).then(result => {
        res.status(200).json({
            status: true,
            message: 'Task berhasil diubah',
        })
    });
})

app.delete('/:id', (req, res) => {
    Task.findByIdAndDelete(req.params.id).then(result => {
        res.status(200).json({
            status: true,
            message: 'Task berhasil dihapus',
        })
    })
})

app.listen(port, () => {
    console.log('server is running on port' + port)
})