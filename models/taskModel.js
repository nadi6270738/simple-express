const mongoose = require('mongoose')

const taskSchema = mongoose.Schema({
    task: {
        type: String,
        required: true
    },
    status: {
        type: String,
        enum: ['Selesai', 'Proses', 'Belum Selesai'],
        default: 'Belum Selesai'
    }
}, {
    timestamps: true
})

const task = mongoose.model('task', taskSchema)
module.exports = task